# Rupiah in denomination (Tokopedia)

This project created follow by Tokopedia instruction to write a simple application that given a number of rupiahs will calculate the minimum number of rupiahs needed to make that amount.

This project developed using [Next.js](https://nextjs.org/ "Next.js").

## Live Demo
You could access **https://denomination-tokopedia.herokuapp.com/**

## How to run
```sh
npm install
npm run dev

// or

yarn
yarn dev
```

## Screenshot
![](https://cdn.pbrd.co/images/HZSnVEX.png)

---
Copyright © 2019 by Muhammad Al Anis Faishal
